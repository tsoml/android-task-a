package eu.tsoml.androidtaska;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import eu.tsoml.androidtaska.databinding.ActivityTask1Binding;

public class Task1 extends AppCompatActivity {

    private ActivityTask1Binding binding;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTask1Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.btnSetText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.tvHelloWord.setText(getResources().getString(R.string.hello_word));
            }
        });

        binding.brnClrText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.tvHelloWord.setText("");
            }
        });

    }
}
