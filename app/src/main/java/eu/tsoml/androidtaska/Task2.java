package eu.tsoml.androidtaska;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import eu.tsoml.androidtaska.databinding.ActivityTask2Binding;

public class Task2 extends AppCompatActivity {

    private ActivityTask2Binding binding;
    private int value = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTask2Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.btnIncrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.tvCounter.setText(String.valueOf(++value));
            }
        });

        binding.btnDecrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.tvCounter.setText(String.valueOf(--value));
            }
        });

    }
}
